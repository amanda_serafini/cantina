<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->truncate();

       User::create([
            'nome' => 'admin',
            'cpf'=>'111.111.111-11',
            'rg'=>'1.111.111',
            'telefone'=>'(11) 1111-1111',
            'celular'=>'(11) 1111-11111',
            'endereco'=>'Videira',
            'cep'=>'89560-000',
            'email'=>'admin@admin.com.br',
            'datanascimento'=>'1111-11-11',
            'funcao'=>'1',
            'password'=>bcrypt('d3v3l0pm3nt'),
        ]);
    }
}
