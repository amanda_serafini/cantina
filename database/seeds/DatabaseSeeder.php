<?php

use Illuminate\Database\Seeder;
use App\user;


class DatabaseSeeder extends Seeder {
 
 
    public function run()
    {
        $this->call('UserTableSeeder');
    }
}
