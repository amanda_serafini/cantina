<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('cpf')->unique();
            $table->string('rg')->unique();
            $table->string('telefone');
            $table->string('celular')->nullable();
            $table->string('endereco');
            $table->string('cep');
            $table->date('datanascimento');
            $table->enum('funcao',['gerente', 'vendedor']);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
