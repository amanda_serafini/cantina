<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSales extends Migration
{

    public function up()
    {   
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('valortotal', 5,2);
            $table->integer('quantidade')->unsigned();
            $table->string('nome');
            $table->enum('tipovenda',['V', 'P']);
            $table->integer('codCliente')->unsigned()->nullable();
            $table->foreign('codCliente')->references('id')->on('clients');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
