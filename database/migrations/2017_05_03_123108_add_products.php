<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->decimal('valor', 5,2);
            $table->integer('quantidade')->unsigned();
            $table->string('descricao');
            $table->string('fornecedor');
            $table->enum('tipo',['bebida', 'salgado','doce']);
            $table->timestamps();
        });
    }

  
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
