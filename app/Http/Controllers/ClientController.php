<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\client;
use App\Sale;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permissaoCliente');
    }

    public function index(Request $request)
    {
        $nome = $request->input('nome');
        $clientes = client::where('nome','LIKE',"%$nome%")->get();
        return view('clients/index',['clientes' => client::where('nome','LIKE',"%$nome%")->get()]);
    }
    
    public function validacao(Request $request){
        $rg = $request->input('rg');
        $cpf = $request->input('cpf');
        $email = $request->input('email');
        $bdrg =  client::where('rg','LIKE',"$rg")->select('rg')->get();
        $bdcpf =  client::where('cpf','LIKE',"$cpf")->select('cpf')->get();
        $bdemail =  client::where('email','LIKE',"$email")->select('email')->get();

        if (count($bdrg) == 0) $validarg = true;
        else $validarg = false; 

        if (count($bdcpf) == 0) $validacpf = true;
        else $validacpf = false; 

        if (count($bdemail) == 0) $validaemail = true;
        else $validaemail = false; 

        return response()->json(['validarg' => $validarg, 'validacpf' => $validacpf, 'validaemail' => $validaemail]);
    }

    public function validacaoeditar(Request $request){
        $id = $request->input('id');
        $rg = $request->input('rg');
        $cpf = $request->input('cpf');
        $email = $request->input('email');
        $bdrg =  client::where('rg','LIKE',"$rg")->where('id','!=',"$id")->select('rg')->get();
        $bdcpf =  client::where('cpf','LIKE',"$cpf")->where('id','!=',"$id")->select('cpf')->get();
        $bdemail =  client::where('email','LIKE',"$email")->where('id','!=',"$id")->select('email')->get();

        if (count($bdrg) == 0) $validarg = true;
        else $validarg = false; 

        if (count($bdcpf) == 0) $validacpf = true;
        else $validacpf = false; 

        if (count($bdemail) == 0) $validaemail = true;
        else $validaemail = false; 

        return response()->json(['validarg' => $validarg, 'validacpf' => $validacpf, 'validaemail' => $validaemail, 'id' => $id]);
    }

    public function create()
    {
        return view('clients/create',['clientes' => client::all()]);
    }

    public function show($id)
    {
        return view('clients/show',['clientes' => client::find($id)]);
    }

    public function edit($id)
    {
        return view('clients/edit',['clientes' => client::find($id)]);
    }

    public function store(Request $request)
    {
        $clientes = new client();
        $clientes->nome = $request->input('nome');
        $clientes->cpf = $request->input('cpf');
        $clientes->rg = $request->input('rg');
        $clientes->telefone= $request->input('telefone');
        $clientes->celular= $request->input('celular');
        $clientes->endereco= $request->input('endereco');
        $clientes->cep= $request->input('cep');
        $clientes->email= $request->input('email');
        $clientes->datanascimento= $request->input('datanascimento');

        if($clientes->save()){
            $request->session()->flash('message', 'Cliente cadastrado com sucesso.');
        } else{
            $request->session()->flash('message', 'Houve um erro ao cadastrar o cliente.');
        }
        return redirect()->route('clientes.index');
    }

    public function update (Request $request, $id){
        $clientes = client::find($id);
        $clientes->nome = $request->input('nome');
        $clientes->cpf = $request->input('cpf');
        $clientes->rg = $request->input('rg');
        $clientes->telefone= $request->input('telefone');
        $clientes->celular= $request->input('celular');
        $clientes->endereco= $request->input('endereco');
        $clientes->cep= $request->input('cep');
        $clientes->email= $request->input('email');
        $clientes->datanascimento= $request->input('datanascimento');

        if($clientes->save()){
            $request->session()->flash('message', 'Cliente atualizado com sucesso.');
        } else{
            $request->session()->flash('message', 'Houve um erro ao atualizar o cliente.');
        }
        return redirect()->route('clientes.index');
    }

    public function destroy(Request $request, $id){
        $clientes = client::find($id);
        $vendas = Sale::where('codCliente','=',$id)->select('id')->get();

        if (count($vendas) > 0){
            $request->session()->flash('message', 'Cliente não pode ser excluido por estar associado a alguma venda');
            return redirect()->route('clientes.index');
        }
        
        
        if($clientes->delete()){
            $request->session()->flash('message', 'Cliente removido com sucesso');
        } else{
            $request->session()->flash('message', 'Houve um erro ao apagar o Cliente.');
        }
        return redirect()->route('clientes.index');
    }
}
