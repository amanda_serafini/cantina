<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Component\HttpFoundation\Response;
use App\Sale; 
use App\client;
use App\Product; 
use App\ProductSale; 

class SaleController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permissaoVenda');
    }

    public function cobranca(Request $request){
        $nome = $request->input('nome');        
        return view('sales/cobranca',['clientes' => client::where('nome','LIKE',"%$nome%")->get()]);
    }
    public function cobrar($id){
        return view('sales/cobrar',['cliente' =>  client::find($id)]);
    }
    public function cobrancaefetuada(Request $request, $id){
        $cliente = client::find($id);
        $cliente->valortotal = $cliente->valortotal - $request->input('valorpago');
        if($cliente->save()){
            $request->session()->flash('message', 'Cobrança efetuada com sucesso.');
        } else{
            $request->session()->flash('message', 'Houve um erro ao processar a cobrança.');
        }
        return redirect()->route('vendas.cobranca');
    }
    public function index(Request $request) {
        $nome = $request->input('nome');       
        return view ('sales/index',['vendas' => Sale::where('nome','LIKE',"%$nome%")->get()]);
    }
    public function index1(Request $request) {
        $nome = $request->input('nome');       
        $request->session()->flash('message', 'Venda atualizada com sucesso');
        return view ('sales/index',['vendas' => Sale::where('nome','LIKE',"%$nome%")->get()]);
    }


    public function create() {
        return view ('sales/create',['clientes' => client::all(),'vendas'=>  new Sale(), 'produtos' => Product::all()]);
    }
    
    public function store(Request $request)
    {
        $produtos = json_decode($request->input('produtos'));
        $tipovenda = $request->input('tipovenda');
        $sale = new Sale();
        $cliente = client::find($request->input('codCliente'));     //obter o cliente para pegar o nome dele
        $sale->nome = $cliente->nome;                               //pegar o nome do cliente
        $sale->codCliente = $request->input('codCliente');
        $sale->tipovenda = $tipovenda;

        //calcular valor total e quantidade da venda
        foreach ($produtos as $prod) {
            $sale->quantidade += $prod->quantidade;
            $aux = Product::find($prod->id);
            $sale->valortotal += $prod->quantidade * $aux->valor;
        }
        if ($tipovenda == 'P') {
            $cliente->valortotal = $cliente->valortotal + $sale->valortotal;
            $cliente->update();
        }
        $sale->save();

        $idvenda = Sale::max('id');
        foreach ($produtos as $prod) {
            //cadastrar os produtos com o id da venda
            $novoproduto = new ProductSale();
            $novoproduto->idvenda = $idvenda; 
            $novoproduto->idproduto = $prod->id;
            $novoproduto->qtdproduto = $prod->quantidade;
            $novoproduto->save();
            //arrumar estoque do produto de acordo com a venda
            $estoque = Product::find($prod->id);
            $estoque->quantidade -= $prod->quantidade;
            $estoque->save();
        }

        return response()->json(['sucesso' => true]);
    }   
    
    public function show($id)
    {
        $venda = Sale::find($id);
        $aux =  DB::table('product_sales')
            ->join('products', 'products.id', '=', 'product_sales.idproduto')->where('idvenda','=',$id)
            ->select('products.valor','products.nome', 'product_sales.qtdproduto')
            ->get();
        return view('sales/show',['venda' => $venda, 'aux' => $aux]);
    }

    public function edit($id)
    {
        $venda = Sale::find($id);
        $produtos = DB::table('product_sales')
            ->join('products', 'products.id', '=', 'product_sales.idproduto')->where('idvenda','=',$id)
            ->select('products.valor','products.nome', 'product_sales.qtdproduto', 'product_sales.idproduto')
            ->get();
        return view ('sales/edit',['vendas'=> $venda, 'produtos' => Product::all(),'produtosvenda' => $produtos]);
    }

    
    public function update(Request $request, $id)
    {
        $venda = Sale::find($id);
        $produtos = ProductSale::all();
        $produtosvendaatual = json_decode($request->input('produtos'));
        $tipovenda = $request->input('tipovenda');
        $cliente = client::find($venda->codCliente);     //obter o cliente para pegar o nome dele
        //diminui o valor da conta se ele comprou a prazo a outra
        if ($venda->tipovenda == 'P'){
            $cliente->valortotal = $venda->valortotal - $venda->valortotal;
            $cliente->update();
        }
        $venda->valortotal = 0;
        $venda->quantidade = 0;
        //calcula quantidade e valor total da venda nova
        foreach ($produtosvendaatual as $prod) {
            $venda->quantidade += $prod->quantidade;
            $aux = Product::find($prod->id);
            $venda->valortotal += $prod->quantidade * $aux->valor;
        }
        //adiciona o tipo dessa venda
        $venda->tipovenda = $tipovenda;
        //se for a prazo aumenta esse valor na venda
        if ($venda->tipovenda == 'P') {
            $cliente->valortotal = $cliente->valortotal + $venda->valortotal;
            $cliente->update();
        }
        //deleta os produtos das venda anterior
        foreach ($produtos as $prod) {
            if ($prod->idvenda == $id) {
                //aumenta no estoque os produtos que tavam nessa venda
                $aux = Product::find($prod->idproduto);
                $aux->quantidade += $prod->qtdproduto;
                $aux->update();
                $prod->delete();
            }
        }
        //salva os produtos da venda atual
        foreach ($produtosvendaatual as $prod) {
            $novoproduto = new ProductSale();
            $novoproduto->idvenda = $id; 
            $novoproduto->idproduto = $prod->id;
            $novoproduto->qtdproduto = $prod->quantidade;
            $novoproduto->save();
            //diminui o estoque com o que foi comprado
            $estoque = Product::find($prod->id);
            $estoque->quantidade -= $prod->quantidade;
            $estoque->save();
        }
        $venda->save();
        return response()->json(['sucesso' => $id]);
    }

    public function destroy(Request $request, $id)
    {
        $venda = Sale::find($id);
        $produtos = ProductSale::all();
        foreach ($produtos as $prod) {
            if ($prod->idvenda == $id) {
                $prod->delete();
            }
        }
        if($venda->delete()){
            $request->session()->flash('message', 'Venda excluida com sucesso');
        } else{
            $request->session()->flash('message', 'Houve um erro ao apagar a venda.');
        }
        return redirect()->route('vendas.index');
    }
}
