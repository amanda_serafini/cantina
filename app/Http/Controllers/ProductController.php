<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product; 

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth'); 
        $this->middleware('permissaoProdutos');
    }

    public function stock(Request $request){
        $nome = $request->input('nome');        
        return view('products/stock',['produtos' => Product::where('nome','LIKE',"%$nome%")->get()]);
    }
    
    public function stockEdit($id){
        return view('products/stockEdit',['produto' =>  Product::find($id)]);
    }

    public function stockUpdate(Request $request, $id){
        $produto = Product::find($id);
        $produto->quantidade = $produto->quantidade + $request->input('quantidade');

        if($produto->save()){
            $request->session()->flash('message', 'Estoque atualizado com sucesso.');
        } else{
            $request->session()->flash('message', 'Houve um erro ao editar o estoque.');
        }
        return redirect()->route('produtos.stock');
    }
    
    public function index(Request $request){
        $nome = $request->input('nome');        
        return view('products/index',['produto' => Product::where('nome','LIKE',"%$nome%")->get()]);
    }

    public function show($id){
        return view('products/show',['produto' => Product::find($id)]);
    }
    public function edit($id){
        return view('products/edit',['produto' => Product::find($id)]);
    }

    public function create(){
        $produto = new Product();
        return view('products/create',['produto' => $produto]);
    }

    public function store(Request $request){
        $produto = new Product();
        $produto->nome = $request->input('nome');
        $produto->valor = $request->input('valor');
        $produto->fornecedor = $request->input('fornecedor');
        $produto->tipo = $request->input('tipo');
        $produto->quantidade = $request->input('quantidade');
        $produto->descricao = $request->input('descricao');

        if($produto->save()){
            $request->session()->flash('message', 'Produto cadastrado com sucesso.');
        } else{
            $request->session()->flash('message', 'Houve um erro ao cadastrar o produto.');
        }
        return redirect()->route('produtos.index');
    }
    
    public function update (Request $request, $id){
        $produto = Product::find($id);
        $produto->nome = $request->input('nome');
        $produto->valor = $request->input('valor');
        $produto->fornecedor = $request->input('fornecedor');
        $produto->tipo = $request->input('tipo');
        $produto->quantidade = $request->input('quantidade');
        $produto->descricao = $request->input('descricao');

        if($produto->save()){
            $request->session()->flash('message', 'Produto atualizado com sucesso.');
        } else{
            $request->session()->flash('message', 'Houve um erro ao editar o produto.');
        }
        return redirect()->route('produtos.index');
    }

    public function destroy(Request $request, $id){
        $produto = Product::find($id);
        if($produto->delete()){
            $request->session()->flash('message', 'Produto removido com sucesso');
        } else{
            $request->session()->flash('message', 'Houve um erro ao apagar o produto.');
        }
        return redirect()->route('produtos.index');
    }

}
