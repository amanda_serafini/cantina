<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\user;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permissaoUsuarios');
    }

    public function validacao(Request $request){
        $rg = $request->input('rg');
        $cpf = $request->input('cpf');
        $email = $request->input('email');
        $bdrg =  user::where('rg','LIKE',"$rg")->select('rg')->get();
        $bdcpf =  user::where('cpf','LIKE',"$cpf")->select('cpf')->get();
        $bdemail =  user::where('email','LIKE',"$email")->select('email')->get();

        if (count($bdrg) == 0) $validarg = true;
        else $validarg = false; 

        if (count($bdcpf) == 0) $validacpf = true;
        else $validacpf = false; 

        if (count($bdemail) == 0) $validaemail = true;
        else $validaemail = false; 

        return response()->json(['validarg' => $validarg, 'validacpf' => $validacpf, 'validaemail' => $validaemail]);
    }

    public function validacaoeditar(Request $request){
        $id = $request->input('id');
        $rg = $request->input('rg');
        $cpf = $request->input('cpf');
        $email = $request->input('email');
        $bdrg =  user::where('rg','LIKE',"$rg")->where('id','!=',"$id")->select('rg')->get();
        $bdcpf =  user::where('cpf','LIKE',"$cpf")->where('id','!=',"$id")->select('cpf')->get();
        $bdemail =  user::where('email','LIKE',"$email")->where('id','!=',"$id")->select('email')->get();

        if (count($bdrg) == 0) $validarg = true;
        else $validarg = false; 

        if (count($bdcpf) == 0) $validacpf = true;
        else $validacpf = false; 

        if (count($bdemail) == 0) $validaemail = true;
        else $validaemail = false; 

        return response()->json(['validarg' => $validarg, 'validacpf' => $validacpf, 'validaemail' => $validaemail]);
    }

    public function index(Request $request)
    {
        $nome = $request->input('nome');
        return view('users/index',['usuarios' => User::where('nome','LIKE',"%$nome%")->get()]);
    }

    public function create()
    {
        $usuarios = new User();
        return view('users/create',['usuarios' => new User()]);
    }

    public function show($id)
    {
        return view('users/show',['usuarios' => User::find($id)]);
    }
        
    public function edit($id)
    {
        return view('users/edit',['usuarios' => User::find($id)]);
    }

    public function store(Request $request)
    {
        $usuarios = new User();
        $usuarios->nome = $request->input('nome');
        $usuarios->cpf = $request->input('cpf');
        $usuarios->rg = $request->input('rg');
        $usuarios->telefone= $request->input('telefone');
        $usuarios->celular= $request->input('celular');
        $usuarios->endereco= $request->input('endereco');
        $usuarios->cep= $request->input('cep');
        $usuarios->email= $request->input('email');
        $usuarios->datanascimento= $request->input('datanascimento');
        $usuarios->funcao=$request->input('funcao');
        $usuarios->password = Hash::make($request->input('password'));

        if($usuarios->save()){
            $request->session()->flash('message', 'Usuario cadastrado com sucesso.');
        } else{
            $request->session()->flash('message', 'Houve um erro ao cadastrar o usuário.');
        }
        return redirect()->route('usuarios.index');
    }

    public function update (Request $request, $id){
        $usuarios = User::find($id);
        $usuarios->nome = $request->input('nome');
        $usuarios->cpf = $request->input('cpf');
        $usuarios->rg = $request->input('rg');
        $usuarios->telefone= $request->input('telefone');
        $usuarios->celular= $request->input('celular');
        $usuarios->endereco= $request->input('endereco');
        $usuarios->cep= $request->input('cep');
        $usuarios->email= $request->input('email');
        $usuarios->datanascimento= $request->input('datanascimento');
        $usuarios->funcao= $request->input('funcao');
        $usuarios->password = Hash::make($request->input('password'));

        if($usuarios->save()){
            $request->session()->flash('message', 'Usuário atualizado com sucesso.');
        } else{
            $request->session()->flash('message', 'Houve um erro ao atualizar o usuário.');
        }
        return redirect()->route('usuarios.index');
    }

    public function destroy(Request $request, $id){
        $usuarios = User::find($id);
        if($usuarios->delete()){
            $request->session()->flash('message', 'Usuario removido com sucesso');
        } else{
            $request->session()->flash('message', 'Houve um erro ao apagar o Usuario.');
        }
        return redirect()->route('usuarios.index');
    }
}
