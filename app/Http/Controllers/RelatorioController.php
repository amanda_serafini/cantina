<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale; 

class RelatorioController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        //$this->middleware('PermissaoUsuarios');
    }

    public function gerarrelatorio(Request $request){
        $datainicial = $request->input('datainicial');
        $datafinal = $request->input('datafinal');
        $vendas = Sale::whereBetween('created_at', ["$datainicial 00:00:00", "$datafinal 23:59:59"])->get();
        $valor = 0;
        foreach ($vendas as $venda) {
            $valor += $venda->valortotal;
        }
        
        return view ('relatorios/relatorios',['vendas' => $vendas,'valor' => $valor]);
    }
}
