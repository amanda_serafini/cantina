<?php

namespace App\Http\Middleware;

use Closure;

class PermissaoVenda
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->funcao != "gerente" && $request->isMethod('delete')) {
            $request->session()->flash('message1', 'Permissão negada! É necessário ter perfil de gerente para realizar esta operação!');
            return redirect('/vendas');
            //falta o alerta de negaçao de permissão
        }

        
        return $next($request);
    }
}
