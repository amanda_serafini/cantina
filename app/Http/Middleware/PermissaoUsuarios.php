<?php

namespace App\Http\Middleware;

use Closure;

class PermissaoUsuarios
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->funcao != "gerente") {
            $request->session()->flash('message1', 'Permissão negada! É necessário ter perfil de gerente para realizar esta operação!');
            return redirect('/');
        }
        return $next($request);
    }
}
