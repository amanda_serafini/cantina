<?php

namespace App\Http\Middleware;

use Closure;

class PermissaoCliente
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->funcao != "gerente" && $request->isMethod('delete')) {
            $request->session()->flash('message1', 'Permissão negada! É necessário ter perfil de gerente para realizar esta operação!');
            return redirect('/clientes');
        }
        return $next($request);
    }
}
