# Projeto Cantina

Esse projeto tem por objetivo avaliar o processo de desenvolvimento de um software na disciplina de Engenharia de Software do Curso de Ciência da Computação do Instituto Federal Catarinense- Campus Videira.
### Sistema Operacional: Windows/Linux (Pode ser que de erro de permissão, só dar a permissão) ###
## Dependências

* PHP >= 5.5
* Mysql Server >= 5.5.54
* Laravel 5.4
* Composer

## Passos para execução


* Clonar o arquivo no diretório escolhido.
* Executar o comando **composer install** no terminal.
* Executar o comando **composer update** no terminal.
* Criar um arquivo *.env* ao lado do .env.example
* Copiar do arquivo .env.example e colar no .env informando o nome da database, do usuario e a senha do mysql.
* Executar o comando **php artisan key:generate** no terminal para gerar a chave de transação do banco de dados.
* Criar a database de acordo com o nome desejado e informado no arquivo .env utilizando o **collation utf8mb4 - utf8mb4_unicode_ci**.
* Executar o comando **php artisan migrate** no terminal para geração do banco.
* Executar o comando **composer dump-autoload** para permitir o seed do banco.
* Executar o comando **php artisan db:seed** para gerar o usuario admin para acesso inicial do sistema.


## Links Úteis
* Laravel: https://laravel.com
* Jquery API: https://api.jquery.com
* Composer: https://getcomposer.org
* Bootstrap: http://getbootstrap.com
* MySQL: https://dev.mysql.com/doc/
* Git: https://git-for-windows.github.io/

## Para o caso de erros
* StackOverflow: https://pt.stackoverflow.com
* Google: https://www.google.com.br

## Na pior das hipóteses contate os admin do projeto.
####**Para saber a senha do admin olhe o UserTableSeeder.php**