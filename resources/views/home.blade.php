@extends('layouts.app')
@section('content')

<script>
    setTimeout(function () {
        $('#Mensagem').hide();
        var url = "/";    
        $(location).attr('href',url);
    }, 3000);
</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="margin-top:250px">
                <div class="panel-heading" style="text-align:center; font-size:20px">ATENÇÃO!</div>

                <div class="panel-body" style="text-align:center; font-size:15px">
                    Login efetuado! Você será redirecionado em instantes...
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
