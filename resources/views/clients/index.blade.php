@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
            setTimeout(function () {
                $('#Mensagem').hide();
            }, 3000);
            $(document).ready(function () {
                $('.delete').click(function () { 
                    if (! confirm("Confirmar essa operação?")){
                        return false;
                    }
                });    
            });

            
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Clientes</h2>
                <div class="panel panel-default" style="margin-top: 20px; padding-left:50px; padding-right:50px;">
                    <div class="panel-body">
                        <a href="/" class="btn pull-left">
                            Voltar
                        </a>
                        <a href="/clientes/create" class="btn pull-right"  style="margin-left:5px;">
                            Novo Cliente
                        </a>
                        <a href="/vendas/create" class="btn pull-right" >
                            Nova Venda
                        </a>

                    </div>
                    <div>
                        @if(Session::has('message'))
                        <div class="alert alert-success" id="Mensagem">
                            <em>{!! session('message')!!}</em>
                        </div>
                        @endif
                        @if(Session::has('message1'))
                        <div class="alert alert-danger" id="Mensagem">
                            <em>{!! session('message1')!!}</em>
                        </div>
                        @endif
                        <hr/>
                        {{Form::open(array('route' => array('clientes.index'), 'method' => 'GET'))}}
                        {{Form::label('nome', 'Digite o nome do cliente:',['class' => 'col-lg-3 control-label', 'style' => 'margin-top:4px'])}}
                        {{Form::text('nome',null,['class' => 'col-lg-7', 'style' => 'border-radius:4px', 'required'])}}
                        {{Form::submit('Consultar', array('class' => 'btn pull-right' ,'style' => 'margin-top:-5px; margin-right:50px;'))}} 
                        {{Form::close() }}
                    </div>
                    <div class="row">
                        </br></br></br>
                        <table class="table table-striped table-bordered table-responsive" style="font-size: 15px">
                            <tr>
                                <th style="text-align: center;">Nome</th>
                                <th style="text-align: center;">Telefone</th>
                                <th style="text-align: center;">Celular</th>
                                <th style="text-align: center;">E-mail</th>
                                <th style="text-align: center;">Valor devido</th>
                                <th style="text-align: center;">Detalhes</th>
                                <th style="text-align: center;">Editar</th>
                                <th style="text-align: center;">Remover</th>
                            </tr>
                            @foreach($clientes as $cliente)
                            <tr style="text-align: center;">
                                <td>{{$cliente->nome}}</td>
                                <td>{{$cliente->telefone}}</td>
                                <td>{{$cliente->celular}}</td>
                                <td>{{$cliente->email}}</td>
                                <td>R$ {{$cliente->valortotal}}</td>          
                                <td>
                                    <a href = "/clientes/{{$cliente->id}}" class="btn btn-default cor" aria-label="Mostrar Cliente">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </a>
                                </td>
                                <td>
                                    <a href = "/clientes/{{$cliente->id}}/edit" class="btn btn-default cor" aria-label="Editar Cliente">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </td>
                                <td>
                                    {{ Form::open(array('url' => 'clientes/' . $cliente->id)) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"> </span>', array('class'=>'btn btn-default delete','type'=>'submit')) }}
                                    {{Form::close()}}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>