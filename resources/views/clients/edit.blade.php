@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

         <script>
            validacao = true;
            function validar(validacao){
                var id = "{{$clientes->id}}";
                var rg = $("#rg").val();
                var cpf = $('#cpf').val();
                var email = $('#email').val();
                
                $.ajax({
                    type: 'GET',
                    url: "/clientes/validacaoeditar",
                    data: {'_token': '{{ csrf_token() }}', rg: rg, cpf: cpf, email: email, id: id},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log('Deu Boa:', data);
                        v = validarvalidacao(data, validacao);
                        return v;
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
            function validarvalidacao(data, validacao){
                if (data.validarg == false){
                    console.log('deu ruim rg')
                    $('#validarg').show();
                    var validacao = false;
                }
                if (data.validacpf == false){
                    console.log('deu ruim cpf')
                    $('#validacpf').show();
                    validacao = false;
                }
                if (data.validaemail == false){
                    console.log('deu ruim email')
                    $('#validaemail').show();
                    validacao = false;                         
                }
                if (validacao == true) {
                    $('#form-submit').click();
                }
            }

            $(document).ready(function(){
                $("#validar").click(function (e) {
                    e.preventDefault();
                    validar(validacao);
                });

                $('#cpf').mask('999.999.999-99');
                $('#rg').mask('9.999.999');
                
                jQuery("#telefone")
                    .mask("(99) 9999-9999?9")
                    .focusout(function (event) {  
                        var target, phone, element;  
                        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                        phone = target.value.replace(/\D/g, '');
                        element = $(target);  
                        element.unmask();  
                        if(phone.length > 10) {  
                            element.mask("(99) 99999-999?9");  
                        } else {  
                            element.mask("(99) 9999-9999?9");  
                        }  
                });

                 jQuery("input#celular")
                    .mask("(99) 9999-9999?9")
                    .focusout(function (event) {  
                        var target, phone, element;  
                        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                        phone = target.value.replace(/\D/g, '');
                        element = $(target);  
                        element.unmask();  
                        if(phone.length > 10) {  
                            element.mask("(99) 99999-999?9");  
                        } else {  
                            element.mask("(99) 9999-9999?9");  
                        }  
                });
                $('#cep').mask('99999-999');
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Edição de Cliente</h2>
                <div class="panel panel-default" style="margin-top: 50px; margin-left: 150px; margin-right:150px; padding:20px; ">
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin-left:60px;">
                            {{Form::model($clientes, array('route' => array('clientes.update',$clientes->id), 'method' => 'PUT'))}}
                            <div class="row">
                                <div class="col-lg-12 ">
                                    <div class="form-group">
                                        {{Form::label('nome', 'Nome:',['class' => 'col-lg-2 control-label'])}}
                                        {{Form::text('nome',null,['class' => 'col-lg-8' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o nome do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('cpf','CPF:',['class' => 'col-lg-4 control-label'])}}
                                        {{Form::text('cpf',null,['class' => 'col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o CPF do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                        <span class="help-block" id="validacpf" style="display:none">
                                            <strong>CPF já cadastrado</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('rg','RG:',['class' => 'col-lg-2 control-label'])}}
                                        {{Form::text('rg',null,['class' => 'col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o RG do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                        <span class="help-block" id="validarg" style="display:none">
                                            <strong>Rg já cadastrado</strong>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('telefone','Telefone:',['class' => 'col-lg-4 control-label'])}}
                                        {{Form::text('telefone' ,null,['class' => 'col-lg-6','style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o telefone do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('celular','Celular:  ',['class' => 'col-lg-2 control-label'])}}
                                        {{Form::text('celular' ,null,['class' => 'col-lg-6' ,'style' => 'border-radius:4px'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('email','E-mail:',['class' => 'col-lg-4 control-label'])}}
                                        {{Form::email('email' ,null,['class' => 'col-lg-6' ,'style' => 'border-radius:4px'])}}
                                        <span class="help-block" id="validaemail" style="display:none">
                                            <strong>Email já cadastrado</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('datanascimento','Nascimento:',['class' => 'col-lg-3 control-label'])}}
                                        {{Form::date('datanascimento' ,null,['class' => 'col-lg-5' ,'style' => 'border-radius:4px', 'required',
                                         'oninvalid' => 'this.setCustomValidity("Informe o nome do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('endereco','Endereço:',['class' => 'col-lg-4 control-label'])}}
                                        {{Form::text('endereco'  ,null,['class' => 'col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                         'oninvalid' => 'this.setCustomValidity("Informe o endereço do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('cep','CEP:',['class' => 'col-lg-2 control-label'])}}
                                        {{Form::text('cep' ,null,['class' => 'col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                         'oninvalid' => 'this.setCustomValidity("Informe o CEP do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                            </div>
                            </br></br>
                            <div class="form-group col-lg-12">
                                <div class="col-lg-6">
                                    <a href="/clientes" class="btn" style="margin-left:-230px">
                                        Voltar
                                    </a>
                                </div>
                                <div class="col-lg-6">
                                    {{Form::reset('Restaurar', array('class' => 'btn', 'style'=>'margin-left:80px'))}}
                                    {{Form::submit('Atualizar', array('class' => 'btn', 'id' => 'validar', 'style' => 'margin-right:70px'))}}
                                    {{Form::submit('Salvar2', array('class' => 'btn', 'id' => 'form-submit', 'style' => 'margin-right:-115px; display:none'))}}
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>


