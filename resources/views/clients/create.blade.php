@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
            validacao = true;
            function validar(validacao){
                var rg = $("#rg").val();
                var cpf = $('#cpf').val();
                var email = $('#email').val();
                
                $.ajax({
                    type: 'GET',
                    url: "/clientes/validacao",
                    data: {'_token': '{{ csrf_token() }}', rg: rg, cpf: cpf, email: email},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log('Deu Boa:', data);
                        v = validarvalidacao(data, validacao);
                        return v;
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
            function validarvalidacao(data, validacao){
                if (data.validarg == false){
                    console.log('deu ruim rg')
                    $('#validarg').show();
                    var validacao = false;
                }
                if (data.validacpf == false){
                    console.log('deu ruim cpf')
                    $('#validacpf').show();
                    validacao = false;
                }
                if (data.validaemail == false){
                    console.log('deu ruim email')
                    $('#validaemail').show();
                    validacao = false;                         
                }
                if (validacao == true) {
                    $('#form-submit').click();
                }
            }


            $(document).ready(function(){
                $("#validar").click(function (e) {
                    e.preventDefault();
                    validar(validacao);
                });

                $('#cpf').mask('999.999.999-99');
                $('#rg').mask('9.999.999');
                $('#cep').mask('99999-999');

                jQuery("#telefone")
                    .mask("(99) 9999-9999?9")
                    .focusout(function (event) {  
                        var target, phone, element;  
                        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                        phone = target.value.replace(/\D/g, '');
                        element = $(target);  
                        element.unmask();  
                        if(phone.length > 10) {  
                            element.mask("(99) 99999-999?9");  
                        } else {  
                            element.mask("(99) 9999-9999?9");  
                        }  
                });

                 jQuery("input#celular")
                    .mask("(99) 9999-9999?9")
                    .focusout(function (event) {  
                        var target, phone, element;  
                        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                        phone = target.value.replace(/\D/g, '');
                        element = $(target);  
                        element.unmask();  
                        if(phone.length > 10) {  
                            element.mask("(99) 99999-999?9");  
                        } else {  
                            element.mask("(99) 9999-9999?9");  
                        }  
                });
             });
        </script>
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Cadastro de Cliente</h2>
                <div class="panel panel-default" style="margin-top: 50px; margin-left: 150px; margin-right:150px; padding:20px; ">
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin-left:60px;">
                            {{Form::model($clientes, array('route' => array('clientes.store'), 'id' => 'form-cliente'))}}
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        {{Form::label('nome', 'Nome:',['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label'])}}
                                        {{Form::text('nome',null,['class' => 'col-xs-8 col-sm-8 col-md-8 col-lg-8' ,'style' => 'border-radius:4px','required', 
                                        'oninvalid' => 'this.setCustomValidity("Informe o nome do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('cpf','CPF:',['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'])}}
                                        {{Form::text('cpf',null,['class' => 'col-xs-6 col-sm-6 col-md-6 col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o CPF do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                        <span class="help-block" id="validacpf" style="display:none">
                                            <strong>CPF já cadastrado</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('rg','RG:',['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label'])}}
                                        {{Form::text('rg',null,['class' => 'col-xs-6 col-sm-6 col-md-6 col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o RG do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                        <span class="help-block" id="validarg" style="display:none">
                                            <strong>Rg já cadastrado</strong>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row" >
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('telefone','Telefone:',['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'])}}
                                        {{Form::text('telefone' ,null,['class' => 'col-xs-6 col-sm-6 col-md-6 col-lg-6','style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o telefone do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                   
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('celular','Celular:  ',['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label'])}}
                                        {{Form::text('celular' ,null,['class' => 'col-xs-6 col-sm-6 col-md-6 col-lg-6' ,'style' => 'border-radius:4px',])}}
                                    </div>
                                </div>
                            </div>

                            <div class="row" >
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('email','E-mail:',['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'])}}
                                        {{Form::email('email' ,null,['class' => 'col-xs-6 col-sm-6 col-md-6 col-lg-6' ,'style' => 'border-radius:4px'])}}
                                        <span class="help-block" id="validaemail" style="display:none">
                                            <strong>Email já cadastrado</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('datanascimento','Nascimento:',['class' => 'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label'])}}
                                        {{Form::date('datanascimento' ,null,['class' => 'col-xs-5 col-sm-5 col-md-5 col-lg-5' ,'style' => 'border-radius:4px', 'required',
                                         'oninvalid' => 'this.setCustomValidity("Informe a data de nascimento do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                            </div>

                            <div class="row" >
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('endereco','Endereço:',['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'])}}
                                        {{Form::text('endereco'  ,null,['class' => 'col-xs-6 col-sm-6 col-md-6 col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                         'oninvalid' => 'this.setCustomValidity("Informe o endereço do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('cep','CEP:',['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label'])}}
                                        {{Form::text('cep' ,null,['class' => 'col-xs-6 col-sm-6 col-md-6 col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o CEP do cliente")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                            </div>
                            </br></br>
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="/clientes" class="btn btnv" style="margin-left:-160px">
                                        Cancelar
                                    </a>
                                    {{Form::reset('Limpar', array('class' => 'btn' ))}}
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    {{Form::submit('Salvar', array('class' => 'btn', 'id' => 'validar', 'style' => 'margin-right:-115px'))}}
                                    {{Form::submit('Salvar2', array('class' => 'btn', 'id' => 'form-submit', 'style' => 'margin-right:-115px; display:none'))}}
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>
