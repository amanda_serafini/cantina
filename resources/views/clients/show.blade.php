@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    
    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h1>Clientes</h1>
                <div class="panel panel-default" style="margin-top: 50px; padding-left:20px; padding-right:20px;">
                    <div class="panel-body">

                        <div class="row" style="margin-top:20px;">
                            <div class="col-lg-6">
                                <table class="table table-bordered table-striped table-responsive" style="text-align:center;">
                                    <tr>
                                        <th>Nome</th>
                                        <td>{{$clientes->nome}}</td>
                                    </tr>
                                    <tr>
                                        <th>CPF</th>
                                        <td>{{$clientes->cpf}}</td>
                                    </tr>
                                    <tr>
                                        <th>RG</th>
                                        <td>{{$clientes->rg}}</td>
                                    </tr>
                                    <tr>
                                        <th>Telefone</th>
                                        <td>{{$clientes->telefone}}</td>
                                    </tr>
                                    <tr>
                                        <th>Celular</th>
                                        <td>{{$clientes->celular}}</td>
                                    </tr>
                                    <tr>
                                        <th>Endereço</th>
                                        <td>{{$clientes->endereco}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-bordered table-striped table-responsive" style="text-align:center">
                                    <tr>
                                        <th>CEP</th>
                                        <td>{{$clientes->cep}}</td>
                                    </tr>
                                    <tr>
                                        <th>E-mail</th>
                                        <td>{{$clientes->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>Data de nascimento</th>
                                        <td>{{$clientes->datanascimento}}</td>
                                    </tr>
                                    <tr>
                                        <th>Valor Total Devido</th>
                                        <td>R$ {{$clientes->valortotal}}</td>
                                    </tr>
                                    <tr>
                                        <th>Criado em </th>
                                        <td>{{$clientes->created_at->format('d/m/Y h:i')}}</td>
                                    </tr>
                                    <tr>
                                        <th>Atualizado em</th>
                                        <td>{{$clientes->updated_at->format('d/m/Y h:i')}}</td>
                                    </tr>
                                </table>
                            </div>
                            <a href="/clientes" class="btn pull-left" style="margin-left: 15px; margin-top:20px">
                                Voltar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
  </body>
</html>

