<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
            setTimeout(function () {
                $('#Mensagem').hide();
            }, 3000);
        </script>

        
    </head>
    <body>
        @extends('layout')
        @section('content')
       
        <div class="container-fluid">
            <div class="col-lg-12">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    </br></br></br></br>
                    <div class="row">
                        <div class="col-lg-12" style="text-align: center;">
                            <h2>Cantina IFC - Campus Videira</h2>
                        </div>
                    </div>
                    </br></br>
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading"></div>
                            <div class="panel-body">
                                <div class="row">
                                    @if(Session::has('message1'))
                                    <div class="alert alert-danger" id="Mensagem" style="text-align:center">
                                        <em>{!! session('message1')!!}</em>
                                    </div>
                                    @endif
                                    
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 btn " style="margin-top:20px">
                                        <a href="/vendas" >
                                            <h4>Vendas</h4>
                                        </a>
                                    </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 btn" style="margin-top:20px">
                                        <a href="/usuarios">
                                            <h4>Usuários</h4>
                                        </a>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 btn" style="margin-top:20px">
                                        <a>
                                            <a href="/clientes">
                                                <h4>Clientes</h4>
                                            </a>
                                        </a>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 btn" style="margin-top:20px">
                                        <a href="/produtos">
                                            <h4>Produtos</h4>
                                        </a>
                                    </div>
                                      <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 btn" style="margin-top:20px">
                                        <a href="/vendas/cobranca">
                                            <h4>Cobrança</h4>
                                        </a>
                                    </div>
                                      <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 btn" style="margin-top:20px">
                                        <a href="/produtos/estoque"> 
                                            <h4>Estoque</h4>
                                        </a>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 btn" style="margin-top:20px">
                                        <a href="/relatorios">
                                            <h4>Relatório</h4>
                                        </a>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>
