<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Projeto Cantina</title>
        <!--<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <script src="bootstrap.min.js"></script> -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script> 

        <style>
            .panel{
                background-color:#F8F8FF;
            }

            .btn{
                background-color:#008B8B;
                color: white !important;
            }

            .btn:hover{
                color: white !important;
            }

            .btnv{
                background-color:#B22222;
                color: white !important;
            }

            .glyphicon-remove{
                color: black !important;
            }

            a:link {
                color: white;
            }

            a:hover {
                text-decoration:none !important;
                color: white !important;
            }

            a:visited{
                color: white;
            }

            a:focus{
                text-decoration:none !important;
                color: white !important;
            }

            .cor{
                color: black !important;
            }

            .cor:hover{
                color: black !important;
            }

            .navbar .container-fluid ul li a{
                color:white;
                text-decoration:none !important;
            }

            .navbar .container-fluid .navbar-header .navbar-brand{
                color:white;
            }

            ul li ul a:hover {
                text-decoration:none !important;
                color: #2F4F4F !important;
            }

        </style>
        <script >
            $(document).ready(function () {
                $(".ativo").click(function () { 
                    $(".ativo").css({ backgroundColor: "#008B8B" });

            });
            });
            
        </script>

    </head>
    <body style="background-color:#F0F8FF">
        @yield('content')
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid" style="background-color: #008B8B;">
                <div class="navbar-header" >
                    <div class="navbar-brand">Cantina IFC</div>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="/">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle ativo" data-toggle="dropdown" href="#">Clientes
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu ativo">
                            <li style=""><a href="/clientes/create">Adicionar clientes</a></li>
                            <li><a href="/clientes"> Consultar clientes</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle ativo"  data-toggle="dropdown" href="#">Usuários
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu ativo">
                            <li><a href="/usuarios/create">Adicionar usuário</a></li>
                            <li><a href="/usuarios">Consultar usuários</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle ativo" data-toggle="dropdown" href="#">Vendas
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu ativo">
                            <li class="destaque"><a href="/vendas/create">Nova venda</a></li>
                            <li><a href="/vendas">Consultar vendas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle ativo" data-toggle="dropdown" href="#">Produtos
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu ativo">
                            <li><a href="/produtos/create">Novo Produto</a></li>
                            <li><a href="/produtos">Consultar produtos</a></li>
                        </ul>
                    </li>
                    <li>
                        <a  href="/vendas/cobranca">Cobrança</a>
                    </li>
                    <li>
                        <a  href="/produtos/estoque">Estoque</a>
                    </li>
                        <li>
                        <a  href="/relatorios">Relatório</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown pull-right">
                        <a href="#" class="dropdown-toggle ativo" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->nome }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu ativo" role="menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    Sair
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    
                    </li>
                </ul>
                    
            </div>
        </nav>
    </body>
</html>
