@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
            setTimeout(function () {
                $('#Mensagem').hide();
            }, 3000);
        </script>
        
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Cobrança</h2>
                <div class="panel panel-default" style="margin-top: 40px; margin-left: 160px; margin-right:160px; padding:20px; ">
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin-left:180px;">
                            <div class="row">
                                <a href="/" class="btn pull-left" style="margin-left:-170px">
                                    Voltar
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <hr/>
                            {{Form::open(array('route' => array('vendas.cobranca'), 'method' => 'GET'))}}
                            {{Form::label('nome', 'Digite o nome do cliente:',['class' => 'col-lg-4 control-label', 'style' => 'margin-top:4px'])}}
                            {{Form::text('nome',null,['class' => 'col-lg-5', 'style' => 'border-radius:4px', 'required'])}}
                            {{Form::submit('Consultar', array('class' => 'btn pull-right' ,'style' => 'margin-top:-5px; margin-right:50px;'))}}
                            {{ Form::close() }}
                        </div>
                        </br>
                        <div class="row">
                            @if(Session::has('message'))
                            <div class="alert alert-success" id="Mensagem">
                                <em>{!! session('message')!!}</em>
                            </div>
                            @endif
                            <table class="table table-striped table-responsive table-bordered" style="font-size:15px"> 
                                <tr>
                                    <th style="text-align:center">Nome</th>
                                    <th  style="text-align:center">Valor Devido</th>
                                    <th  style="text-align:center">Cobrar</th>
                                </tr>
                                @foreach($clientes as $cliente)
                                <?php
                                    if ($cliente->valortotal > 0){
                                ?>
                                    <tr  style="text-align:center">
                                        <td>{{$cliente->nome}}</td>
                                        <td>{{$cliente->valortotal}}</td>
                                        <td>
                                            <a href = "/vendas/{{$cliente->id}}/cobrar" class="btn btn-default cor" aria-label="Editar Produto">
                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                            </a>
                                        </td>
                                    </tr>

                                <?php
                                    }
                                ?>
                                
                                @endforeach      
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>