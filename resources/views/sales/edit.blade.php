@extends('layout')
@section('content')
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>
          .tooltip.inner {
              background-color: #73AD21; 
              padding: 15px;
              font-size: 12px;
          }
      </style>
      <script>
        //localstorage é limpo ao abrir a página
        localStorage.clear();
      </script>
      @foreach($produtosvenda as $aux)
        <script>
          var nomeProduto = "{{$aux->nome}}";
          var quantidade = "{{$aux->qtdproduto}}";
          var valorProduto = "{{$aux->valor}}";
          var idProduto = "{{$aux->idproduto}}";
          console.log("Quantidade: " + quantidade + ", nome:  " + nomeProduto + ", id: " + idProduto + ", valor: "  + valorProduto);
          var produtos = JSON.parse(localStorage.getItem('produtos'));
          var tbProdutos = JSON.parse(localStorage.getItem('tbProdutos'));
          if (produtos == null) {
            produtos = [];
          } 
          if (tbProdutos == null) {
            tbProdutos = [];
          }
          produtos.push({'id' : idProduto, 'quantidade' : quantidade});
          tbProdutos.push({'id' : idProduto, 'quantidade' : quantidade, 'nomeProduto' : nomeProduto, 'valorProduto' : valorProduto});
          localStorage['produtos'] = JSON.stringify(produtos);
          localStorage['tbProdutos'] = JSON.stringify(tbProdutos);
        </script>
      @endforeach

      <script type="text/javascript">
        //enviar json via ajax para salvar os produtos
        function send() {
          var produtos = localStorage.getItem('produtos');
          var tipovenda = $('#tipovenda').val();
          //se não tiver produto cadastrado
          if (produtos == null || JSON.parse(produtos) == 0){
              $('select#produtos').tooltip("show");
              return;
          }
          if (confirm("Confirmar esta operação?")){
            $.ajax({
              type: 'PATCH',
              url: "/vendas/{{$vendas->id}}",
              data: {'_token': '{{ csrf_token() }}', produtos: produtos, tipovenda: tipovenda},
              dataType: 'JSON',
              success: function (data) {
                console.log('Deu Boa:', data);
                var url = "/vendas/index";    
                $(location).attr('href',url);
              },
              error: function (data) {
                  console.log('Error:', data);
              }
            });
          }
        }
        //lista os valores 
        function Listar(){
          $('select#produtos').tooltip("hide");
          var tbProdutos = JSON.parse(localStorage.getItem('tbProdutos'));
          var tabela = $('table#compra tbody');
          var tabela1 = $('table#compra');
          tabela.empty();
          tabela1.append(
            "<tr>" +
            "<th></th>"  +
            "<th>Nome</th>"  +
            "<th>Quantidade</th>" +
            "<th>Valor Unitario</th>" +
            "<th>Valor Total</th>" +
            "<th>Excluir</th>" +
            "</tr>"
          );
          for(var i in tbProdutos){
            var prod = tbProdutos[i];
            tabela.append("<tr><td>" + i + 
                          "</td><td>" + prod.nomeProduto + 
                          "</td><td>" + prod.quantidade + 
                          "</td><td>" + prod.valorProduto +
                          "</td><td>" + (parseFloat(prod.valorProduto) * parseFloat(prod.quantidade)).toFixed(2) +
                          "</td><td> <span alt='" + i + "' qtd='" + prod.quantidade + "' id='" + prod.id + 
                            "' class='btn btn-default glyphicon glyphicon-remove remover' ></span></td></tr>");
              }
          }

        //exclui o item do localstorage
        function Excluir(selecionado){
          var tbProdutos = JSON.parse(localStorage.getItem('tbProdutos'));
          var produtos = JSON.parse(localStorage.getItem('produtos'));
          $('#valortotal').val(parseFloat($('#valortotal').val()) - (parseFloat(tbProdutos[selecionado].valorProduto) * parseFloat(produtos[selecionado].quantidade)));
          $('#quantidadetotal').val(parseInt($('#quantidadetotal').val()) - parseInt(produtos[selecionado].quantidade));
          tbProdutos.splice(selecionado, 1);
          produtos.splice(selecionado, 1);
          localStorage.setItem("tbProdutos", JSON.stringify(tbProdutos));
          localStorage.setItem("produtos", JSON.stringify(produtos));
          console.log("Registro excluido.");
        }
          $(document).ready(function () {
              //colocar valor maximo no produto de acordo com estoque
              $("select#produtos option:selected").each(function () {
                  $("#quantidade").attr("max", $(this).data('maximo'));
              });
              //se possuir algo no localstorage ele lista
              if (JSON.parse(localStorage.getItem('tbProdutos')) != null) {
                  Listar();
              }
              $(".clientes").select2({
                  placeholder: "Selecione um cliente",
              }
              );
              $(".tipovenda").select2({});
              $(".produtos").select2({
                  placeholder: "Selecione o produto",
              }
              );
              //ao selecionar um produto coloca o valor maximo dele com a quantidade do estoque
              $('#produtos').on('select2:select', function (evt) {
                  $("select#produtos option:selected").each(function () {
                      if ($(this).attr('maximo') == 0){
                          $("#quantidade").val('');    
                          $("#quantidade").attr('disabled','disabled');
                      }
                      else{
                          $("#quantidade").removeAttr('disabled','disabled');
                          $("#quantidade").val(1);
                          $("#quantidade").attr("max", $(this).attr('maximo'));
                      }
                  });
              });
              //ao mudar a quantidade
              $('#quantidade').click(function () { 
                  $('#quantidade').tooltip("destroy");
              });
              //ao clicar para remover
              $("#compra").on("click", ".remover", function () {
                var selecionado = parseInt($(this).attr("alt"));
                var qtdatual = parseInt($(this).attr("qtd"));
                var idproduto = parseInt($(this).attr("id"));
                var a = parseInt($("option#prod"+idproduto).attr('maximo')) + parseInt(qtdatual);
                $('option#prod'+ idproduto).attr('maximo', a);
                $("#quantidade").attr("max", parseInt($("option#prod"+idproduto).attr('maximo')));
                Excluir(selecionado);
                Listar();
                $("select#produtos option:selected").each(function () {
                      if ($(this).attr('maximo') == 0){
                          $("#quantidade").val('');    
                          $("#quantidade").attr('disabled','disabled');
                      }
                      else{
                          $("#quantidade").removeAttr('disabled','disabled');
                          $("#quantidade").val(1);
                          $("#quantidade").attr("max", $(this).attr('maximo'));
                      }
                  });
              });
              //ao clicar para salvar
              $("#update").click(function () {
                  send();       //enviar via ajax
              });
              //ao clicar nos botoes de sair o localstorage é limpo
              $('.encerra').click(function () {
                  localStorage.clear();
              });
              //ao clicar para adicionar
              $("#add-produto").click(function () {
                  $("select#produtos option:selected").each(function () {
                    if (! $("#quantidade").val() == ""){
                      var nomeProduto = $(this).text();
                      var idProduto = $(this).val();
                      var estoque = $(this).attr('maximo');
                      var valorProduto = $(this).data('valor');
                      var quantidade = $("#quantidade").val();

                      var total = $(this).attr('maximo') - quantidade;
                      if (total < 0){
                          $('#quantidade').tooltip("show");
                      }
                      else{
                        $(this).attr('maximo', total);
                        $("#quantidade").val(1);
                        //se acabar o estoque
                        if ($(this).attr('maximo') <= 0){
                            $("#quantidade").val('');    
                            $("#quantidade").attr('disabled','disabled');
                        }
                        else {
                            $("#quantidade").attr("max", $(this).attr('maximo'));
                        }

                        console.log("Quantidade: " + quantidade + ", nome:  " + nomeProduto + ", id: " + idProduto + ", valor: " + valorProduto);
                        var produtos = JSON.parse(localStorage.getItem('produtos'));
                        var tbProdutos = JSON.parse(localStorage.getItem('tbProdutos'));
                        if (produtos == null) {
                            produtos = [];
                        }
                        if (tbProdutos == null) {
                            tbProdutos = [];
                        }
                        produtos.push({'id': idProduto, 'quantidade': quantidade});
                        tbProdutos.push({'id': idProduto, 'quantidade': quantidade, 'nomeProduto': nomeProduto, 'valorProduto': valorProduto});
                        localStorage['produtos'] = JSON.stringify(produtos);
                        localStorage['tbProdutos'] = JSON.stringify(tbProdutos);
                        Listar();
                        var aux = parseFloat(valorProduto) * parseFloat(quantidade) + parseFloat($('#valortotal').val());
                        $('#valortotal').val(aux.toFixed(2));
                        $('#quantidadetotal').val(parseInt(quantidade) + parseInt($('#quantidadetotal').val()));
                      }
                    }
                  });
              });
          });
      </script>
  </head>

  <body>
    <div class="container">
      <div class="row" style="margin-top: 80px; text-align:center;">
              <h2>Edição da Venda</h2>
              <div class="panel panel-default" style="margin-top: 50px; margin-left: 150px; margin-right:150px; padding-left:20px; padding-right:20px">
                  <div class="panel-body">
                    <div class="form-horizontal">
                      <a class="btn pull-right encerra" href='/clientes/create'>Cadastrar Cliente</a>
                    </div>
                    </br></br>
                    <div class="row">
                      <hr/>
                      <div class="form-horizontal" >
                        {{Form::model($vendas, array('route' => array('vendas.update',$vendas->id)))}}
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                              {{Form::label('codCliente', 'Nome:',['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'])}}
                              <select readonly="readonly" name="codCliente" id="cliente" class="clientes col-xs-6 col-sm-6 col-md-6 col-lg-6" data-toggle="tooltip" title="Selecione um cliente!" data-placement="top">
                                  <option>{{$vendas->nome}}</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group" class="">
                              {{Form::label('tipovenda','Tipo de Pagamento:',['class' => 'col-xs-5 col-sm-5 col-md-5 col-lg-5 control-label', 'style'=>'margin-right:-18px'])}}
                              {{Form::select('tipovenda', array( 'V' => 'A vista', 'P' => 'A prazo'), 'V',array('class' => 'tipovenda col-xs-6 col-sm-6 col-md-6 col-lg-6'))}}
                            </div>
                          </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class"form-group">
                              {{Form::label('produto', 'Produtos:',['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4s control-label', 'style' => 'margin-left:10px'])}}
                              <select name="produtos" id="produtos" class="produtos col-xs-8 col-sm-8 col-md-8 col-lg-8" style="width:215px" data-toggle="tooltip" title="Adicione ao menos um produto!" data-placement="top">
                                @foreach($produtos as $produto)
                                  <?php
                                      if ($produto->quantidade > 0) {
                                  ?>
                                    <option value="{{$produto->id}}" id="prod{{$produto->id}}" maximo="{{$produto->quantidade}}" data-valor="{{$produto->valor}}">{{$produto->nome}}</option>        
                                  <?php
                                      }
                                  ?>              
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div>
                              {{Form::label('quantidade','Quantidade:',['class' => 'col-xs-5 col-sm-5 col-md-5 col-lg-5 control-label'])}}
                              {{Form::number('quantidade','1',['min' => '1' ,'class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2', 'style'=> 'border-radius:4px', 'data-toggle' => 'tooltip',
                                  'title' => 'Quantidade Invalida', 'data-placement' => 'top'])}}
                              {{Form::button('Adicionar Produto', array('class'=>'btn', 'id' => 'add-produto', 'style' => 'width:140px')) }}
                            </div>
                          </div>
                        </div>
                        <br/>
                        <table class="table table-bordered table-responsive table-striped" id="compra">
                          <tr></tr>
                        </table>
                        <br/>
                        
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                              {{Form::label('quantidadetotal','Quantidade Total:',['class' => 'col-xs-5 col-sm-5 col-md-5 col-lg-5 control-label'])}}
                              {{Form::number('quantidadetotal',$vendas->quantidade,['readonly', 'class' => 'col-lg-3', 'style'=> 'border-radius:4px'])}}
                            </div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                              {{Form::label('valortotal','Valor Total:',['class' => 'col-xs-5 col-sm-5 col-md-5 col-lg-5 control-label'])}}
                              {{Form::number('valortotal',null,['readonly','step' => '0.01' ,'class' => 'col-xs-3 col-sm-3 col-md-3 col-lg-3', 'style'=> 'border-radius:4px'])}}
                            </div>
                          </div>
                        </div>

                        <div class="row" style="margin-top:20px">
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <a href="/vendas" class="encerra btn btnv" style="margin-left:-200px">
                              Cancelar
                            </a>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            {{Form::button('Salvar', array('class' => 'btn', 'id' => 'update', 'style'=>'margin-left:260px'))}}
                            {{Form::close()}}
                          </div>
                        </div>

                    </div>
                </div>
              </div>
          </div>
      </div>
    </div>
  </body>
</html>
@endsection