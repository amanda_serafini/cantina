@extends('layout')
@section('content')


<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
        $(document).ready(function () {    
            //ocultar a mensagem após certo tempo        
            setTimeout(function () {
                $('#Mensagem').hide();
            }, 3000);
            //ao clicar para editar validar se a venda pode ser efetuada
            $('.editar').click(function () { 
                var id = parseInt($(this).attr("id"));
                var data = new Date($(this).attr("data"));
                var dataatual = new Date("<?=date('Y/m/d H:i:s');?>");
                var tempodiferenca = Math.abs(dataatual.getTime() - data.getTime());
                var horas = Math.ceil(tempodiferenca / (1000 * 3600)); 
                var url = "/vendas/" + id + "/edit"; 
                if ( horas >= 72) {
                    alert("Esta venda não pode ser alterada");
                }   
                else {
                    $(location).attr('href',url);                    
                }
            });
      
                $('.delete').click(function () { 
                    if (! confirm("Confirmar essa operação?")){
                        return false;
                    }
                });    
            });
        </script>

    </head>
    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Vendas</h2>

                <div class="panel panel-default" style="margin-top: 50px; padding-left:20px; padding-right:20px;">
                    <div class="panel-body">

                        <a href="/" class="btn pull-left">
                            Voltar 
                        </a>
                        <a href="/vendas/create" class="btn pull-right">
                            Nova venda
                        </a>
                    </div>
                    <div class="row">
                        <hr/>
                        {{Form::open(array('route' => array('vendas.index'), 'method' => 'GET'))}}
                        {{Form::label('nome', 'Digite o nome do cliente:',['class' => 'col-lg-3 control-label', 'style' => 'margin-top:4px'])}}
                        {{Form::text('nome',null,['class' => 'col-lg-7', 'style' => 'border-radius:4px', 'required'])}}
                        {{Form::submit('Consultar', array('class' => 'btn pull-right' ,'style' => 'margin-top:-5px; margin-right:50px;'))}}
                        {{ Form::close() }}
                        </br></br>
                    </div>
                    <div class="row">
                        @if(Session::has('message'))
                        <div class="alert alert-success" id="Mensagem">
                            <em>{!! session('message')!!}</em>
                        </div>
                        @endif
                        @if(Session::has('message1'))
                        <div class="alert alert-danger" id="Mensagem">
                            <em>{!! session('message1')!!}</em>
                        </div>
                        @endif

                        <table class="table table-striped table-responsive table-bordered" style="font-size:15px"> 
                            <tr>
                                <th style="text-align: center;">Cliente</th>
                                <th style="text-align: center;">Valor Total</th>
                                <th style="text-align: center;">Quantidade Total</th>
                                <th style="text-align: center;">Detalhes</th>
                                <th style="text-align: center;">Editar</th>
                                <th style="text-align: center;">Remover</th>
                            </tr>
                            @foreach($vendas as $venda)
                                <tr style="text-align: center;">
                                    <td>{{$venda->nome}}</td>
                                    <td>{{$venda->valortotal}}</td>
                                    <td>{{$venda->quantidade}}</td>

                                    <td>
                                        <a href = "/vendas/{{$venda->id}}" class="btn btn-default cor" aria-label="Mostrar Venda">
                                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                    <td>
                                        <a id="{{$venda->id}}" data="{{$venda->created_at}}" class="btn btn-default editar cor" aria-label="Editar Venda">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                   
                                    <td>
                                        {{ Form::open(array('url' => 'vendas/' . $venda->id)) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::button('<span class="glyphicon glyphicon-remove delete" aria-hidden="true"> </span>', array('class'=>'btn btn-default delete', 'type'=>'submit')) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>