@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
            $(document).ready(function() {
            $("#valorpago").change(function() {
                $('#valorrestante').val(parseInt($('#valortotal').val()) - parseInt($('#valorpago').val()))
            });
            $("#valorpago").attr("max", {{$cliente -> valortotal}});
                $('#valorrestante').val(parseInt($('#valortotal').val()) - parseInt($('#valorpago').val()))
            });
        </script>
    </head>
    
    <body>        
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Cobrança</h2>
                <div class="panel panel-default" style="margin-top: 40px; margin-left: 160px; margin-right:160px; padding:20px;">
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin-left:20px;">
                            {{Form::model($cliente, array('route' => array('vendas.cobrar',$cliente->id), 'method' => 'PUT'))}}
                            <div class="form-group">
                                {{Form::label('nome', 'Nome:',['class' => 'col-lg-4 control-label'])}}
                                {{Form::text('nome',null,['readonly' , 'class' => 'col-lg-6' ,'style' => 'border-radius:4px', 'required'])}}
                            </div>

                            <div class="form-group">
                                {{Form::label('valortotal','Valor Devido:',['class' => 'col-lg-4 control-label'])}}
                                {{Form::number('valortotal',null,['readonly' , 'class' => 'col-lg-2' ,'style' => 'border-radius:4px', 'required'])}}
                            </div>

                            <div class="form-group">
                                {{Form::label('valorpago','Valor do pagamento:',['class' => 'col-lg-4 control-label'])}}
                                {{Form::number('valorpago',0,['min' => '0', 'step' => '0.01', 'class' => 'col-lg-2' ,'style' => 'border-radius:4px', 'required'])}}
                            </div>

                            <div class="form-group">
                                {{Form::label('valorrestante','Valor restante:',['class' => 'col-lg-4 control-label'])}}
                                {{Form::number('valorrestante',null,['readonly', 'class' => 'col-lg-2' ,'style' => 'border-radius:4px', 'required'])}}
                            </div>

                            <div class="row" style="margin-top:30px">
                                <div class="col-lg-6">
                                    <a href="/vendas/cobranca" class="btn">
                                        Voltar
                                    </a> 
                                </div>
                                <div class="col-lg-6">
                                    {{Form::reset('Restaurar', array('class' => 'btn'))}}
                                    {{Form::submit('Finalizar', array('class' => 'btn', 'style' => 'margin-right:35px'))}}
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>


