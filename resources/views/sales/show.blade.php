@extends('layout')
@section('content')


<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>

        <div class="container">
            <div class="row" style="margin-top: 90px; text-align:center;">
                <h2>Visualizar venda</h2>
                <div class="panel panel-default" style="margin-top: 50px; padding-left:20px; padding-right:20px;">
                    <div class="panel-body">

                        <div class="row" style="margin-top:40px;">
                            <div class="col-lg-6">
                                <table class="table table-bordered table-striped table-responsive" style="text-align:center">
                                    <tr>
                                        <th>Cliente</th>
                                        <td>{{$venda->nome}}</td>
                                    </tr>
                                    <tr>
                                        <th>Valor Total</th>
                                        <td>{{$venda->valortotal}}</td>
                                    </tr>
                                    <tr>
                                        <th>Quantidade Total</th>
                                        <td>{{$venda->quantidade}}</td>
                                    </tr>
                                    <tr>
                                        <th>Criado em </th>
                                        <td>{{$venda->created_at->format('d/m/Y h:i')}}</td>
                                    </tr>
                                    <tr>
                                        <th>Atualizado em</th>
                                        <td>{{$venda->updated_at->format('d/m/Y h:i')}}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <table class="table table-bordered table-striped table-responsive" style="text-align:center">
                                    <tr>
                                        <th>Produto</th>
                                        <th>Quantidade</th>
                                        <th>Valor Unitário</th>
                                        <th>Valor Total</th>
                                    </tr>
                                    @foreach($aux as $aux)
                                    <tr>
                                        <td>{{$aux->nome}}</td>
                                        <td>{{$aux->qtdproduto}}</td>
                                        <td>{{$aux->valor}}</td>
                                        <td>{{$aux->valor * $aux->qtdproduto}}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>

                        <a href="/vendas" class="btn pull-left" style="margin-top:50px">
                            Voltar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endsection
</body>
</html>