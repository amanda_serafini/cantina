@extends('layout')
@section('content')


<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>
            $(document).ready(function () {    
                //ocultar a mensagem após certo tempo        
                setTimeout(function () {
                    $('#Mensagem').hide();
                }, 3000);

                //ao clicar para editar validar se a venda pode ser efetuada
                $('#gerar').click(function () { 
                    var datainicial = new Date($('#datainicial').val());
                    var datafinal = new Date($('#datafinal').val());
                    var tempodiferenca = datafinal.getTime() - datainicial.getTime();
                    
                    if(tempodiferenca < 0){
                        alert("Selecione uma data inicial anterior que a final")
                        return false;
                    }


                });
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Relatório de Vendas</h2>
                <div class="panel panel-default" style="margin-top: 50px; padding-left:20px; padding-right:20px;">
                    <div class="panel-body">

                        <a href="/" class="btn pull-left">
                            Voltar 
                        </a>
                        {{Form::number('total',$valor,['readonly','class' => 'col-lg-2 pull-right', 'style' => 'border-radius:4px'])}}
                        {{Form::label('data', 'Valor Vendido:',['class' => 'col-lg-3 control-label pull-right', 'style' => 'margin-top:4px'])}}
                        
                    </div>
                    <div class="row">
                        <hr/>
                        {{Form::open(array('route' => array('relatorios.gerar'), 'method' => 'GET'))}}
                        {{Form::label('datainicial', 'Digite a data inicial:',['class' => 'col-lg-2 control-label', 'style' => 'margin-top:4px'])}}
                        {{Form::date('datainicial',null,['class' => 'col-lg-3', 'style' => 'border-radius:4px; text-align:center;', 'required'])}}

                        {{Form::label('datafinal', 'Digite a data final:',['class' => 'col-lg-2 control-label', 'style' => 'margin-top:4px'])}}
                        {{Form::date('datafinal',null,['class' => 'col-lg-3', 'style' => 'border-radius:4px; text-align:center;', 'required'])}}
                        {{Form::submit('Gerar', array('class' => 'btn pull-right', 'id' => 'gerar', 'style' => 'margin-top:-5px; margin-right:50px;'))}}
                        {{ Form::close() }}
                        </br></br>
                    </div>
                    <div class="row">
                        <table class="table table-striped table-responsive table-bordered" style="font-size:15px"> 
                            <tr>
                                <th style="text-align: center;">Data</th>
                                <th style="text-align: center;">Cliente</th>
                                <th style="text-align: center;">Valor Total</th>
                                <th style="text-align: center;">Quantidade Total</th>
                            </tr>
                            @foreach($vendas as $venda)
                                <tr style="text-align: center;">
                                    <td>{{$venda->created_at->format('d/m/Y')}}</td>
                                    <td>{{$venda->nome}}</td>
                                    <td>{{$venda->valortotal}}</td>
                                    <td>{{$venda->quantidade}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>