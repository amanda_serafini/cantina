<!DOCTYPE html>
@extends('layout')
@section('content')

<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
            setTimeout(function () {
                $('#Mensagem').hide();
            }, 3000);


             $(document).ready(function () {
                $('.delete').click(function () { 
                    if (! confirm("Confirmar essa operação?")){
                        return false;
                    }
                });    
            });
        </script>


        
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Usuários</h2>
                <div class="panel panel-default" style="margin-top: 50px; padding-left:20px; padding-right:20px;">
                    <div class="panel-body">

                        <a href="/" class="btn pull-left">
                            Voltar
                        </a>
                        <a href="/usuarios/create" class="btn pull-right">
                            Novo usuário
                        </a>
                        <br/>
                        <div>
                            <hr/>
                            {{Form::open(array('route' => array('usuarios.index'), 'method' => 'GET'))}}
                            {{Form::label('nome', 'Digite o nome do usuário:',['class' => 'col-lg-3 control-label', 'style' => 'margin-top:4px'])}}
                            {{Form::text('nome',null,['class' => 'col-lg-7', 'style' => 'border-radius:4px', 'required'])}}
                            {{Form::submit('Consultar', array('class' => 'btn pull-right' ,'style' => 'margin-top:-5px; margin-right:50px;'))}}
                            {{ Form::close() }}
                            </br></br></br>
                        </div>

                        <div class="row">
                            @if(Session::has('message'))
                            <div class="alert alert-success" id="Mensagem">
                                <em>{!! session('message')!!}</em>
                            </div>
                            @endif


                            <table class="table table-striped table-bordered table-responsive">
                                <tr>
                                    <th style="text-align: center;">Nome</th>
                                    <th style="text-align: center;">Telefone</th>
                                    <th style="text-align: center;">Celular</th>
                                    <th style="text-align: center;">E-mail</th>
                                    <th style="text-align: center;">Função</th>
                                    <th style="text-align: center;">Detalhes</th>
                                    <th style="text-align: center;">Editar</th>
                                    <th style="text-align: center;">Remover</th>
                                </tr>

                                @foreach($usuarios as $usuario)
                                <tr>
                                    <td>{{$usuario->nome}}</td>
                                    <td>{{$usuario->telefone}}</td>
                                    <td>{{$usuario->celular}}</td>
                                    <td>{{$usuario->email}}</td>
                                    <td>{{$usuario->funcao}}</td>


                                    <td>
                                        <a href = "/usuarios/{{$usuario->id}}" class="btn btn-default cor" aria-label="Mostrar Usuário">
                                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                    <td>
                                        <a href = "/usuarios/{{$usuario->id}}/edit" class="btn btn-default cor" aria-label="Editar Usuário">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                    <td>
                                        {{ Form::open(array('url' => 'usuarios/' . $usuario->id)) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"> </span>', array('class'=>'btn btn-default delete', 'type'=>'submit')) }}
                                        {{Form::close()}}
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>