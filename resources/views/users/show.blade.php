@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 70px; text-align:center;">
                <h1>Usuários</h1>
                <div class="panel panel-default" style="margin-top: 50px; padding-left:20px; padding-right:20px;">
                    <div class="panel-body">
                        <div class="row" style="margin-top:40px;">
                            <div class="col-lg-4">
                                <table class="table table-bordered table-striped table-responsive" style="text-align:center;font-size:15px">

                                    <th>Nome</th>
                                    <td>{{$usuarios->nome}}</td>
                                    </tr>
                                    <tr>
                                        <th>CPF</th>
                                        <td>{{$usuarios->cpf}}</td>
                                    </tr>
                                    <tr>
                                        <th>RG</th>
                                        <td>{{$usuarios->rg}}</td>
                                    </tr>
                                    <tr>
                                        <th>Telefone</th>
                                        <td>{{$usuarios->telefone}}</td>
                                    </tr>
                                    <tr>
                                        <th>Celular</th>
                                        <td>{{$usuarios->celular}}</td>
                                    </tr>
                                    <tr>
                                        <th>Endereço</th>
                                        <td>{{$usuarios->endereco}}</td>
                                    </tr>


                                    <tr>
                                        <th>CEP</th>
                                        <td>{{$usuarios->cep}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-8">
                                <table class="table table-bordered table-striped table-responsive" style="text-align:center; font-size:15px">
                                    <tr>
                                        <th>E-mail</th>
                                        <td>{{$usuarios->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>Data de nascimento</th>
                                        <td>{{$usuarios->datanascimento}}</td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <th>Função:</th>
                                        <td>{{$usuarios->funcao}}</td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <th>Senha:</th>
                                        <td>{{$usuarios->password}}</td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <th>Criado em </th>
                                        <td>{{$usuarios->created_at->format('d/m/Y h:i')}}</td>
                                    </tr>
                                    <tr>
                                        <th>Atualizado em</th>
                                        <td>{{$usuarios->updated_at->format('d/m/Y h:i')}}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <a href="/usuarios" class="btn pull-left">
                            Voltar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endsection
</body>
</html>
