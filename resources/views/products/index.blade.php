@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
            setTimeout(function () {
                $('#Mensagem').hide();
            }, 3000);

             $(document).ready(function () {
                $('.delete').click(function () { 
                    if (! confirm("Confirmar essa operação?")){
                        return false;
                    }
                });    
            });
        </script>
        
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Produtos</h2>

                <div class="panel panel-default" style="margin-top: 50px; padding-left:20px; padding-right:20px;">
                    <div class="panel-body">
                        <a href="/" class="btn pull-left">
                            Voltar 
                        </a>
                        <a href="/produtos/create" class="btn pull-right">
                            Novo Produto
                        </a>
                    </div>
                    <div class="row">
                        <hr/>
                        {{Form::open(array('route' => array('produtos.index'), 'method' => 'GET'))}}
                        {{Form::label('nome', 'Digite o nome do produto:',['class' => 'col-lg-3 control-label', 'style' => 'margin-top:4px'])}}
                        {{Form::text('nome',null,['class' => 'col-lg-7', 'style' => 'border-radius:4px', 'required'])}}
                        {{Form::submit('Consultar', array('class' => 'btn pull-right' ,'style' => 'margin-top:-5px; margin-right:50px;'))}}
                        {{ Form::close() }}
                        </br></br>
                    </div>
                    <div class="row">
                        @if(Session::has('message'))
                        <div class="alert alert-success" id="Mensagem">
                            <em>{!! session('message')!!}</em>
                        </div>
                        @endif
                        @if(Session::has('message1'))
                        <div class="alert alert-danger" id="Mensagem">
                            <em>{!! session('message1')!!}</em>
                        </div>
                        @endif
                        <table class="table table-striped table-responsive table-bordered" style="font-size:15px"> 
                            <tr>
                                <th style="text-align: center;">Nome</th>
                                <th style="text-align: center;">Valor</th>
                                <th style="text-align: center;">Quantidade</th>
                                <th style="text-align: center;">Descrição</th>
                                <th style="text-align: center;">Tipo</th>
                                <th style="text-align: center;">Detalhes</th>
                                <th style="text-align: center;">Editar</th>
                                <th style="text-align: center;">Remover</th>
                            </tr>
                            @foreach($produto as $produto)
                            <tr style="text-align: center;">
                                <td>{{$produto->nome}}</td>
                                <td>{{$produto->valor}}</td>
                                <td>{{$produto->quantidade}}</td>
                                <td>{{$produto->descricao}}</td>
                                <td>{{$produto->tipo}}</td>
                                <td>
                                    <a href = "/produtos/{{$produto->id}}" class="btn btn-default cor" aria-label="Mostrar Produto">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </a>
                                </td>
                                <td>
                                    <a href = "/produtos/{{$produto->id}}/edit" class="btn btn-default cor" aria-label="Editar Produto">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </td>
                                <td>
                                    {{ Form::open(array('url' => 'produtos/' . $produto->id)) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"> </span>', array('class'=>'btn btn-default delete', 'type'=>'submit'))}}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection 
    </body>
</html>