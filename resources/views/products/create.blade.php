@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>
            $(document).ready(function () {
                $(".tipo").select2({
                    placeholder: "Selecione um tipo",
                }
                );
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Cadastro de Produto</h2>
                <div class="panel panel-default" style="margin-top: 50px; margin-left: 150px; margin-right:150px; padding:20px; ">
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin-left:60px;">
                            {{Form::model($produto, array('route' => array('produtos.store')))}}
                            <div class="row">
                                <div class="col-lg-12 ">
                                    <div class="form-group">
                                        {{Form::label('nome', 'Nome:',['class' => 'col-lg-2 control-label'])}}
                                        {{Form::text('nome',null,['class' => 'col-lg-8' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o nome do produto")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('valor','Valor:',['class' => 'col-lg-4 control-label'])}}
                                        {{Form::text('valor',null,['class' => 'col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe o valor do produto")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('quantidade','Quantidade:',['class' => 'col-lg-4 control-label'])}}
                                        {{Form::number('quantidade',null,['class' => 'col-lg-4' ,'style' => 'border-radius:4px', 'required',
                                         'oninvalid' => 'this.setCustomValidity("Informe a quantidade do produto")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 ">
                                    <div class="form-group">
                                        {{Form::label('descricao','Descrição:',['class' => 'col-lg-2 control-label'])}}
                                        {{Form::text('descricao',null,['class' => 'col-lg-8' ,'style' => 'border-radius:4px', 'required',
                                        'oninvalid' => 'this.setCustomValidity("Informe a descrição do produto")', 'onFocusOut' => 'setCustomValidity("")'])}}  
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{Form::label('fornecedor','Fornecedor:',['class' => 'col-lg-4 control-label'])}}
                                        {{Form::text('fornecedor',null,['class' => 'col-lg-6' ,'style' => 'border-radius:4px', 'required',
                                         'oninvalid' => 'this.setCustomValidity("Informe o fornecedor do produto")', 'onFocusOut' => 'setCustomValidity("")'])}}
                                    </div>
                                </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            {{Form::label('tipo','Tipo:',['class' => 'col-lg-4 control-label','style'=>'margin-right:-120px'])}}
                                            {{Form::select('tipo',array('doce' => 'Doce', 'salgado' => 'Salgado', 'bebida' => 'Bebida'),null,array('class' => 'tipo col-lg-3', 'style'=>'margin-right:-150px'))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12" style="margin-top:20px">
                                    <div class="col-lg-6">
                                        <a href="/produtos" class="btn btnv" style="margin-left:-150px">
                                            Cancelar
                                        </a>
                                        {{Form::reset('Limpar', array('class' => 'btn'))}}
                                    </div>
                                    <div class="col-lg-6">
                                        {{Form::submit('Salvar', array('class' => 'btn' ,'style' => 'margin-right:-115px'))}}
                                        {{Form::close()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endsection
    </body>
</html>
