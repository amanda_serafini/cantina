@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>
            $(document).ready(function () {
                $('#quantidade').attr('min', parseInt(-$('#quantidadeatual').val()));
                $('#quantidade').change(function () { 
                    var atual = parseInt($('#quantidadeatual').val()) + parseInt($('#quantidade').val());
                    $('#quantidadefinal').val(atual);

                    
                });
            });

        </script>
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 100px; text-align:center;">
                <h2>Quantidade de estoque</h2>
                <div class="panel panel-default" style="margin-top: 50px; margin-left: 160px; margin-right:160px; padding:20px; ">
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin-left:180px;">
                            {{Form::model($produto, array('route' => array('produtos.stockUpdate',$produto->id), 'method' => 'PUT'))}}
                            <div class="form-group">
                                {{Form::label('nome', 'Nome:',['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'])}}
                                {{Form::text('nome',null,['readonly', 'class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4' ,'style' => 'border-radius:4px'])}}                              
                            </div>
                            <div class="form-group">
                                {{Form::label('quantidadeatual','Quantidade Atual:',['class' =>  'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'])}}
                                {{Form::number('quantidadeatual',$produto->quantidade, ['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2','readonly', 'style' => 'border-radius:4px'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('quantidade','Alteração:',['class' => 'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label'])}}
                                {{Form::number('quantidade','0', ['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2', 'style' => 'border-radius:4px'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('quantidadefinal','Quantidade Atualizada:',['class' =>  'col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label', 'readonly'])}}
                                {{Form::number('quantidadefinal','0', ['class' => 'col-xs-2 col-sm-2 col-md-2 col-lg-2', 'style' => 'border-radius:4px', 'readonly'])}}
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top:20px; margin-left:-100px">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="/produtos" class="btn">
                                        Voltar
                                    </a> 
                                </div>
                                
                               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    {{Form::submit('Atualizar', array('class' => 'btn', 'style' => 'margin-left:30px'))}}
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>