@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 90px; text-align:center;">
                <h1>Produtos</h1>
                <div class="row" style="margin-top:40px;">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <table class="table table-bordered table-striped table-responsive" style="text-align:center;">

                            <tr>
                                <th>Nome</th>
                                <td>{{$produto->nome}}</td>
                            </tr>
                            <tr>
                                <th>Valor</th>
                                <td>{{$produto->valor}}</td>
                            </tr>
                            <tr>
                                <th>Quantidade</th>
                                <td>{{$produto->quantidade}}</td>
                            </tr>
                            <tr>
                                <th>Descrição</th>
                                <td>{{$produto->descricao}}</td>
                            </tr>
                            <tr>
                                <th>Tipo</th>
                                <td>{{$produto->tipo}}</td>
                            </tr>
                            <tr>
                                <th>Fornecedor</th>
                                <td>{{$produto->fornecedor}}</td>
                            </tr>
                            <tr>
                                <th>Criado em </th>
                                <td>{{$produto->created_at->format('d/m/Y h:i')}}</td>
                            </tr>
                            <tr>
                                <th>Atualizado em</th>
                                <td>{{$produto->updated_at->format('d/m/Y h:i')}}</td>
                            </tr>
                        </table>
                        <a href="/produtos" class="btn pull-right">
                            Voltar
                        </a>
                        <div class="col-lg-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    </body>
</html>