@extends('layout')
@section('content')

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
             setTimeout(function () {
                $('#Mensagem').hide();
            }, 3000);
        </script>

    </head>

    <body>
        <div class="container">
            <div class="row" style="margin-top: 80px; text-align:center;">
                <h2>Estoque</h2>
                <div class="panel panel-default" style="margin-top: 50px; margin-left: 150px; margin-right:150px; padding:20px; ">
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin-left:60px;">
                            <div class="row">
                                <div class="col-lg-6">
                                    <a href="/" class="btn pull-left" style="margin-left: -50px;">
                                        Voltar
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <hr/>
                                {{Form::open(array('route' => array('produtos.stock'), 'method' => 'GET'))}}
                                {{Form::label('nome', 'Digite o nome do produto:',['class' => 'col-lg-4 control-label', 'style' => 'margin-top:4px'])}}
                                {{Form::text('nome',null,['class' => 'col-lg-5', 'style' => 'border-radius:4px', 'required'])}}
                                {{Form::submit('Consultar', array('class' => 'btn pull-right' ,'style' => 'margin-top:-5px; margin-right:50px;'))}}
                                {{ Form::close() }}

                                </br> </br> </br>
                            </div>
                            <div class="row">
                                @if(Session::has('message'))
                                <div class="alert alert-success" id="Mensagem">
                                    <em>{!! session('message')!!}</em>
                                </div>
                                @endif

                                <table class="table table-striped table-responsive table-bordered" style="font-size:15px"> 
                                    <tr>
                                        <th style="text-align: center;">Nome</th>
                                        <th style="text-align: center;">Valor</th>
                                        <th style="text-align: center;">Quantidade</th>
                                        <th style="text-align: center;">Descrição</th>
                                        <th style="text-align: center;">Tipo</th>
                                        <th style="text-align: center;">Fornecedor</th>
                                        <th style="text-align: center;">Atualizar Estoque</th>
                                    </tr>
                                    @foreach($produtos as $produto)
                                    <tr style="text-align: center;">
                                        <td>{{$produto->nome}}</td>
                                        <td>{{$produto->valor}}</td>
                                        <td>{{$produto->quantidade}}</td>
                                        <td>{{$produto->descricao}}</td>
                                        <td>{{$produto->tipo}}</td>
                                        <td>{{$produto->fornecedor}}</td>
                                        <td>
                                            <a href = "/produtos/{{$produto->id}}/estoque" class="btn btn-default cor" aria-label="Editar Produto">
                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>
