<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/  

Route::get('/', function () {
    return view('welcome');
})->middleware('auth');

Route::get('produtos/estoque', 'ProductController@stock')->name('produtos.stock');
Route::get('produtos/{produto}/estoque', 'ProductController@stockEdit')->name('produtos.stockEdit');
Route::put('produtos/{produto}/estoque', 'ProductController@stockUpdate')->name('produtos.stockUpdate');
Route::get('clientes/validacao', 'ClientController@validacao')->name('clientes.validacao'); 
Route::get('clientes/validacaoeditar', 'ClientController@validacaoeditar')->name('clientes.validacaoeditar'); 

Route::get('usuarios/validacaoeditar', 'UserController@validacaoeditar')->name('usuarios.validacaoeditar'); 
Route::get('usuarios/validacao', 'UserController@validacao')->name('usuarios.validacao'); 

Route::resource('produtos','ProductController');
Route::resource('clientes','ClientController');
Route::get('relatorios', 'RelatorioController@gerarrelatorio')->name('relatorios.gerar');

Route::get('vendas/cobranca', 'SaleController@cobranca')->name('vendas.cobranca');
Route::get('vendas/index', 'SaleController@index1')->name('vendas.index1');
Route::get('vendas/{cliente}/cobrar', 'SaleController@cobrar')->name('vendas.cobrar');
Route::put('vendas/{cliente}/cobrar', 'SaleController@cobrancaefetuada')->name('vendas.cobrancaefetuada');

Route::resource('vendas','SaleController');
Route::resource('usuarios','UserController');

Route::get('relatorios', 'RelatorioController@gerarrelatorio')->name('relatorios.gerar')->middleware('permissaoUsuarios');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

