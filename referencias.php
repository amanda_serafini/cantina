  <html>
    <head>
      <meta charset= "utf-8" />
    </head>
    <body>

  <?php

    $nome = 'Amanda';

    echo 'Hello ' . $nome . '<br />';
    echo "Hello $nome <br />";

    $num1 = 10;
    $num2 = 50;

    echo $num1 + $num2 . '<br />';
    echo $num1 - $num2 . '<br />';
    echo $num1 * $num2 . '<br />';
    echo $num1 / $num2 . '<br />';

    $lista = [];
    $lista[] = 'João';
    $lista[] = 'Maria';
    $lista[] = 'Mohammed';

    for($i = 0; $i < count($lista); $i++) {
      echo "O nome é: $lista[$i] <br />";
    }
  
    foreach($lista as $item) {
      echo "O nome é: $item <br />";
    }

    $teste = true;

    /*
      ==  - igual
      !=  - diferente
      ||  - ou
      &&  - e
      or - ou com baixa procedência
      and -  e com baixa procedência
      === - igual(verificando valor e tipagem)
    */
    if ($teste == 1) {
      echo 'É verdadeiro <br />';
    } else{
      echo 'É falso <br / >';
    }

    echo 'A data e hora atual é: ' . date("d/m/Y H:i:s"); 
    
  ?>

    </body>
  </html>
